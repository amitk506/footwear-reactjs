import { Link } from "react-router-dom";
import Card from "../components/Card";
import dummyImg from '../assets/img/dummy-img.png';

function Product(props){

    const productItems = [
        {
           Image: dummyImg,
           Name: 'Product Item Name 1',
           Vendor: 'Vendor Name 1',
           Price: 200,
           Description: "Product detail page description 1"
        },
        {
           Image: dummyImg,
           Name: 'Product Item Name 2',
           Vendor: 'Vendor Name 2',
           Price: 150,
           Description: "Product detail page description 2"
        },
        {
           Image: dummyImg,
           Name: 'Product Item Name 3',
           Vendor: 'Vendor Name 3',
           Price: 300,
           Description: "Product detail page description 3"
        },
        {
           Image: dummyImg,
           Name: 'Product Item Name 4',
           Vendor: 'Vendor Name 4',
           Price: 100,
           Description: "Product detail page description 4"
        },
        {
           Image: dummyImg,
           Name: 'Product Item Name 5',
           Vendor: 'Vendor Name 5',
           Price: 600,
           Description: "Product detail page description 5"
        }
    ];

    const productItemList = {
        display: 'flex',
    };

    return (
        <div style={productItemList}>
            {
                productItems.map((productItem, index) => (
                    <Card key={index}>
                        <div className="ProductItem">
                            <div className="ProductItem__img">
                                <img src={productItem.Image} alt={productItem.Name} />
                            </div>
                            <h3>{productItem.Name}</h3>
                            <p>{productItem.Vendor}</p>
                            <p>Price: {productItem.Price}</p>
                            <div>
                                <a href="#">Add to Cart</a>&nbsp;&nbsp;&nbsp;&nbsp;
                                <Link to="/product-detail" state={productItem}>View More</Link>
                            </div>
                        </div>
                    </Card>
                ))
            }
        </div>
    )
};

export default Product;