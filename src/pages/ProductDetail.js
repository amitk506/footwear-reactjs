import {useLocation} from 'react-router-dom';

function ProductDetail(){

    const location = useLocation();
    const states = location.state;

    return(
        <div>
            {states && (
                <div className="ProductItem">
                    <div className="ProductItem__img">
                        <img src={states.Image} alt={states.Name} />
                    </div>
                    <h3>{states.Name}</h3>
                    <p>{states.Vendor}</p>
                    <p>Price: {states.Price}</p>
                    <div>
                        <div>Add to Cart</div>
                    </div>
                    <div>{states.Description}</div>
                </div>
            )}
        </div>
    )
};

export default ProductDetail;