import { BrowserRouter, Routes, Route } from 'react-router-dom';

import './style.css';

import Header from './shared/Header';
import Footer from './shared/Footer';
import Home from './pages/Home';
import AboutUs from './pages/AboutUs';
import Product from './pages/Product';
import NotFound from './pages/NotFound';
import ProductDetail from './pages/ProductDetail';

function App() {
  return (
    <>      
      <BrowserRouter>
        <Header />
        <Routes>
          <Route path='/' element={<Home />} />
          <Route index element={<Home />} />
          <Route path='/about-us' element={<AboutUs />} />
          <Route path='/product' element={<Product />} />
          <Route path='/product-detail' element={<ProductDetail />} />
          <Route path='*' element={<NotFound/>} />
        </Routes>
        <Footer />
      </BrowserRouter>
    </>
  );
}

export default App;
