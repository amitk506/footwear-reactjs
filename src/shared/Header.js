import Nav from "./Nav";

function Header(){
    return(
        <header>
            <h1>Footerwear Shop</h1>
            <Nav />
            <hr />
        </header>
    )
};

export default Header;