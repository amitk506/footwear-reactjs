import {NavLink} from 'react-router-dom';
import './Nav.css';

function Nav(){

    const NavItem = [
        {
            name: 'Home',
            to: '/'
        },
        {
            name: 'About Us',
            to: '/about-us'            
        },
        {
            name: 'Product',
            to: '/product',
            submenu: [
                {
                    submenuname: 'Sub 1',
                    to: '/sub-menu1'
                },
                {
                    submenuname: 'Sub 2',
                    to: '/sub-menu2'
                },
                {
                    submenuname: 'Sub 3',
                    to: '/sub-menu3'
                }
            ]
        },
        {
            name: 'Contact',
            to: '/contact'            
        }
    ];

    const NavStyItem = {
        listStyle: 'none', 
        marginRight: '30px'
    }

    return (
        <nav>
            <ul className='menu'>
                {
                    NavItem.map(
                        (NavItemLoop, index) => (
                            <li key={index} style={NavStyItem}>
                                {
                                    NavItemLoop.submenu ? (
                                        <>
                                            <NavLink className={({ isActive }) => (isActive ? 'navItem active' : 'navItem')} to={NavItemLoop.to}>{NavItemLoop.name}</NavLink>
                                            <ul className='subMenu'>
                                                {
                                                   NavItemLoop.submenu.map(
                                                       (subMenuItem, index) => (
                                                           <li key={index}><NavLink  className={({ isActive }) => (isActive ? 'navItem active' : 'navItem')} to={subMenuItem.to}>{subMenuItem.submenuname}</NavLink></li>
                                                       )
                                                   ) 
                                                }
                                            </ul>
                                        </>
                                    ) : (
                                        <NavLink className={({ isActive }) => (isActive ? 'navItem active' : 'navItem')} to={NavItemLoop.to}>{NavItemLoop.name}</NavLink>    
                                    )
                                }                                
                            </li>
                        )
                    )
                }                
            </ul>
        </nav>
    )
};

export default Nav;